#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/t2fs_driver.h"
#include "../include/t2fs.h"
#include "../include/bitmap2.h"
#include "../include/apidisk.h"

// Errors
#define STRING_COPY_ERROR -1
#define STRING_SIZE_ERROR -2
#define MAX_HANDLERS_ERROR -99
#define FILE_NOT_FOUND_ERROR -2
#define INVALID_HANDLER_ERROR -1
#define FILE_ALREADY_EXISTS_ERROR -1
#define FULL_DIRECTORY_ERROR -2
#define DIRECTORY_NOT_FOUND_ERROR -1
#define MALLOC_ERROR -2
#define READ_BLOCK_ERROR -3
#define WRITE_RECORD_BLOCK_ERROR -4
#define FREE_RECORD_ERROR -5

// Configurations
#define MAX_HANDLERS 20

	// NOTE1: handle é o index de fileHandlers


int getBlockFromOffset(const int offset,const t2fs_record_t record);
int pathnameToPath(char* pathname);

int identify2 (char *name, int size){
// NOTE: não tenho certeza se o retorno de erro deveria ser quando 
//      o tamanho da string de identificação for diferente de size;
//      Ou se é quando ele não consegue copiar porque o array não é uma string.
//      Para o primeiro caso, segue a validação comentada abaixo:
//	int nameSize = strlen(name);
//	if(size > nameSize) return STRING_SIZE_ERROR;  

// NOTA2: desta forma, se name não for maior ou igual a size, estoura a aplicação
	char id[] = "Fabio Alves 207304 Pedro Guimaraes 205657";	
    char* copiedString = memcpy(name,id,size);	
    int isCopied = strcmp(copiedString,name);

    if(isCopied != 0) return STRING_COPY_ERROR;

	return 0;
}

FILE2 create2 (char *filename){
	// TODO: iniciar arquivo sem blocos

	int i,j;
	int freeBlock;
	t2fs_record_t records[RECORDS_PER_BLOCK];

	// TODO: separar o filename por /
	// TODO: tratar caso de . 
	// TODO: tratar caso de ..
	// NOTE: isso seria implementado pela pathnameToPath()

	read_records_per_block(currentPath,records); 	

	for(j=0; j< superblock.blockSize;j++){		
		if(records[j].TypeVal == 1 && strcmp(records[j].name, filename)==0) return FILE_ALREADY_EXISTS_ERROR;
	}
	for(j=0; j< superblock.blockSize;j++){		
		if(records[j].TypeVal == 0){
			freeBlock = searchFreeBlock2();						// procura por um bloco livre
			allocBlock2(freeBlock);								// aloca bloco antes livre
			records[j].TypeVal = 1;								// cria record do tipo arquivo
			strcpy(records[j].name, filename);					// associa um nome
			records[j].blocksFileSize = 1;						// do tamanho de 1 bloco
			records[j].bytesFileSize = 1;						// do tamanho de 1 byte
			records[j].dataPtr[0] = freeBlock;					// associa um bloco direto
			records[j].dataPtr[1] = 0;							// aponta segundo bloco direto como invalido
			records[j].singleIndPtr = 0;						// aponta bloco indireto simples como invalido
			records[j].doubleIndPtr = 0;						// aponta bloco indireto duplo como invalido

			for(i=0; i < MAX_HANDLERS; i++){
				if(fileHandlers[i] == NULL){
					fileHandlers[i] = (current_pointer2_t*)malloc(sizeof(current_pointer2_t));
					if(fileHandlers[i] == NULL) return -3;
					fileHandlers[i]->record = records[j];
					fileHandlers[i]->offset = 0;

					int isBlockAlloced = allocBlock2(freeBlock);
					if(isBlockAlloced != 0) return -2;

					int isRecordsWrote = write_records_per_block(currentPath,records);
					if(isRecordsWrote != 0) return -4;

					char eofBuffer[] = "\0";
					int isBlockWrote = write_block(freeBlock,eofBuffer);
					if(isBlockWrote != 0) return -5;

					return i; // handler
				}
			} 
			return MAX_HANDLERS_ERROR;
		}
	}

	return FULL_DIRECTORY_ERROR;
}

int delete2 (char *filename){
    int j;	
    int path;
    int isDeleted = -1;
	t2fs_record_t records[RECORDS_PER_BLOCK];

	// TODO: separar o filename por /
	// TODO: tratar caso de . 
	// TODO: tratar caso de ..
	// NOTE: isso seria implementado pela pathnameToPath()
    
    path = currentPath;                                                         // NOTE: apenas arquivos em currentPath

	read_records_per_block(path,records); 	                                    // le os records do path

	for(j=0; j< superblock.blockSize;j++){	                                    // para cada record	
		if(records[j].TypeVal == 1 && strcmp(records[j].name, filename)==0) {   // Se for arquivo e tiver o mesmo filename
            isDeleted = deallocBlocksForRecord(path,records[j]);                 // libera record
        	read_records_per_block(path,records); 	                            // pega records atualizados do disco
            
            // XXX: deveria mandar a posição dentro do bloco para atualizar o record
            //      mas como não tenho muito tempo e já estou tonto após programar 20h nas últimas 48h
            //      vou simplesmente apagar o nome do record aqui e gravar o bloco todo novamente
            strcpy(records[j].name,"");             
            isDeleted = write_records_per_block(path,records);            
        }
	}
	return isDeleted;
}

FILE2 open2 (char *filename){
	int i,j;
	t2fs_record_t records[RECORDS_PER_BLOCK];

	// TODO: separar o filename por /
	// TODO: tratar caso de . 
	// TODO: tratar caso de ..
	// NOTE: isso seria implementado pela pathnameToPath()

	read_records_per_block(currentPath,records); 	

	for(j=0; j< superblock.blockSize;j++){		
		if(records[j].TypeVal == 1 && strcmp(records[j].name, filename)==0 && records[j].dataPtr[0] > 0){	
			for(i=0; i < MAX_HANDLERS; i++){
				if(fileHandlers[i] == NULL){
					fileHandlers[i] = (current_pointer2_t*)malloc(sizeof(current_pointer2_t));
					fileHandlers[i]->record = records[j];
					fileHandlers[i]->offset = 0;
					return i;
				}
			} 

			return MAX_HANDLERS_ERROR;
		}
	}

	return FILE_NOT_FOUND_ERROR;
}

int close2 (FILE2 handle){
	if(fileHandlers[handle] == NULL) return INVALID_HANDLER_ERROR;
	fileHandlers[handle] = NULL;
	return 0;
}

int read2 (FILE2 handle, char *buffer, int size){
	if(fileHandlers[handle] == NULL) return INVALID_HANDLER_ERROR;

	char* ptrContent;
	int fileReaderBlock, readBlocks = 0, readChars = 0;
	int offset,charsToRead;
	int blocksToRead = 1; // Garante a primeira iteracao no laco

	charsToRead = size;
    blocksToRead = fileHandlers[handle]->record.blocksFileSize;
	offset =  fileHandlers[handle]->offset % (SECTORS_PER_BLOCK*SECTOR_SIZE);

	while(size > 0 && blocksToRead > 0){
		fileReaderBlock = getBlockFromOffset(fileHandlers[handle]->offset,fileHandlers[handle]->record);
        if(fileReaderBlock == -1)
            break;

		ptrContent = malloc(SECTORS_PER_BLOCK*SECTOR_SIZE);
		read_block(fileReaderBlock,ptrContent);
		readBlocks++;

        // le o bloco a partir do offset
		charsToRead = SECTORS_PER_BLOCK*SECTOR_SIZE - offset - charsToRead;
		charsToRead = (charsToRead >= 0 ? size : SECTORS_PER_BLOCK*SECTOR_SIZE - offset);
        
		memcpy(buffer+readChars,ptrContent+offset,charsToRead); 
		readChars += charsToRead-1;
		size -= charsToRead; // atualiza o restante
	
		fileHandlers[handle]->offset += charsToRead; // atualiza offset

		blocksToRead = fileHandlers[handle]->record.blocksFileSize - readBlocks;
        offset = 0;
        charsToRead = size;
	}
    buffer[readChars] = '\0';

	return readChars;
}

int write2 (FILE2 handle, char *buffer, int size){
    // TODO: se reduzir o tamanho do arquivo, desalocar blocos

	if(fileHandlers[handle] == NULL) return INVALID_HANDLER_ERROR;

	char* ptrContent;
	char* readBuffer;
	t2fs_record_t records[RECORDS_PER_BLOCK];
	int fileWriterBlock, wroteBlocks = 0,wroteChars = 0;
    int charsToWrite,blocksToWrite,charsToReplace;
    int isWrote;
    int offset, initialOffset;
    int leftInFile;
    int path;
    int i;

    path = currentPath;

	charsToWrite = size;
    blocksToWrite = fileHandlers[handle]->record.blocksFileSize;
    wroteChars = strlen(buffer);
    initialOffset = fileHandlers[handle]->offset; // guarda offset inicial
    leftInFile = (blocksToWrite*SECTORS_PER_BLOCK*SECTOR_SIZE);
    offset = fileHandlers[handle]->offset % (SECTORS_PER_BLOCK*SECTOR_SIZE);

	while(charsToWrite > 0 && blocksToWrite > 0){ 
	    fileWriterBlock = getBlockFromOffset(fileHandlers[handle]->offset, fileHandlers[handle]->record); // pega bloco atual do offset
        if(fileWriterBlock == -1) 
            fileWriterBlock = allocBlockForRecord(path,fileHandlers[handle]->record);

	    ptrContent = malloc(SECTORS_PER_BLOCK*SECTOR_SIZE); // size -> 1 bloco
	    read_block(fileWriterBlock,ptrContent);
        
	    //buffer recebe somente a partir do offset
	    readBuffer = malloc(SECTORS_PER_BLOCK*SECTOR_SIZE); // size -> 1 bloco
	    memcpy(readBuffer,ptrContent,SECTORS_PER_BLOCK*SECTOR_SIZE); // copia 1 bloco
 
	    // verificar quantos caracteres cabem ainda neste bloco e calcular um novo size para ser inserido
		charsToWrite = SECTORS_PER_BLOCK*SECTOR_SIZE - offset - charsToWrite;
		charsToWrite = (charsToWrite >= 0 ? size : -1*charsToWrite);

	    memcpy(readBuffer+offset,buffer,charsToWrite);	 // insere no meio
        
	    charsToReplace = strlen(ptrContent) - offset;

	    // verificar quantos caracteres cabem ainda neste bloco e calcular um novo size para ser inserido        
		charsToWrite = SECTORS_PER_BLOCK*SECTOR_SIZE - offset - charsToReplace - size;
        charsToWrite = (charsToWrite >= 0 ? charsToReplace : charsToReplace - size);

	    memcpy(readBuffer+offset+size,ptrContent+offset,charsToWrite);                  // conserta o final

	    isWrote = write_block(fileWriterBlock,readBuffer);                              // Escreve arquivo no disco
	    if(isWrote == 0){
		    wroteBlocks++;
		    blocksToWrite = fileHandlers[handle]->record.blocksFileSize - wroteBlocks;

            if(charsToWrite <= charsToReplace){                                          // Se escreveu menos do que deveria
                charsToReplace = charsToReplace - charsToWrite;                           // precisa escrever o resto                                
                buffer = malloc(charsToReplace);
                memcpy(buffer,ptrContent+offset+charsToWrite,charsToReplace);                  // conserta o final
                buffer[charsToReplace] = '\0';  // coloca \0 no bloco novo
                puts(buffer);    
                charsToWrite = charsToReplace;

                offset = 0;
                size = charsToWrite;

                leftInFile -= strlen(readBuffer) + charsToWrite;

                if(blocksToWrite == 0 && leftInFile < 0){                                           // Se ainda tem o que escrever e faltam blocos
                    allocBlockForRecord(path,fileHandlers[handle]->record);                   // aloca novo bloco
            	    read_records_per_block(path,records);                                     // le record atualizado do disco
                    
                	for(i=0; i< superblock.blockSize;i++){		
	                	if(records[i].TypeVal == 1 && strcmp(records[i].name, fileHandlers[handle]->record.name)==0)
                            fileHandlers[handle]->record = records[i];                               // atualiza record do fileHandler
                    }

		            blocksToWrite = fileHandlers[handle]->record.blocksFileSize - wroteBlocks;   // atualiza contador de blocos restantes
                
                    leftInFile += SECTORS_PER_BLOCK*SECTOR_SIZE;

                    buffer[charsToWrite] = '\0';  // coloca \0 no bloco novo;  // coloca \0 no bloco novo
                }
            }
            else {        // já escreveu tudo
                charsToWrite = 0;
                blocksToWrite = 0;
            }
            fileHandlers[handle]->offset = wroteBlocks * superblock.blockSize * SECTOR_SIZE;			
	    }
        else 
            return -1;    
	}
    // Se chegou aqui, isWrote sempre deu sucesso!
    fileHandlers[handle]->offset = initialOffset + wroteChars;                      // atualiza offset
    fileHandlers[handle]->record.bytesFileSize += wroteChars;                       // atualiza contador de bytes do record
    update_record(path,fileHandlers[handle]->record);                               // atualiza record no disco

	return isWrote;
}

int seek2 (FILE2 handle, DWORD offset){
	if(fileHandlers[handle] == NULL) return INVALID_HANDLER_ERROR;
    if(offset == -1)
        offset = fileHandlers[handle]->record.bytesFileSize; // offset do final do arquivo
	fileHandlers[handle]->offset = offset;
	return 0;
}

int mkdir2 (char *pathname){
	int path;
	int j;
	int freeBlock;
    int isRight = 0;
	t2fs_record_t records[RECORDS_PER_BLOCK];
	t2fs_record_t* newRecords;

	// TODO: separar o filename por /
	// TODO: tratar caso de . 
	// TODO: tratar caso de ..
	// NOTE: isso seria implementado pela pathnameToPath()

	path = currentPath;

	read_records_per_block(path,records); 	

	for(j=0; j< superblock.nOfDirEntries;j++){		                        // se diretorio ja existir, retorna ERRO!
		if(records[j].TypeVal == 2 && strcmp(records[j].name, pathname)==0) return FILE_ALREADY_EXISTS_ERROR;	
	}
	for(j=0; j< superblock.nOfDirEntries;j++){						        // senao, cria diretorio
		if(records[j].TypeVal == 0){										// Se o record eh invalido, cria novo record
			freeBlock = searchFreeBlock2();									// busca um bloco livre 
            if(freeBlock == 0)
                return -1;
            
			isRight = allocBlock2(freeBlock);											// aloca o bloco antes livre
            if(isRight != 0)            
                return -1;

			records[j].TypeVal = 2;											// do tipo diretorio
			strcpy(records[j].name, pathname);								// com nome 
			records[j].blocksFileSize = 1;									// um bloco direto
			records[j].bytesFileSize = SECTORS_PER_BLOCK * SECTOR_SIZE;		// do tamanho de 1 bloco
			records[j].dataPtr[0] = freeBlock;								// recebendo o bloco livre
			records[j].dataPtr[1] = 0;										// aponta segundo bloco direto para invalido
			records[j].singleIndPtr = 0;									// aponta bloco indireto simples para invalido
			records[j].doubleIndPtr = 0;									// aponta bloco indireto duplo para invalido
			write_records_per_block(path,records);							// salva records de path no disco

			newRecords = (t2fs_record_t*) malloc(RECORDS_PER_BLOCK*sizeof(t2fs_record_t)); 	// aloca novo record na memoria
			if(newRecords == NULL)															// senao
				return MALLOC_ERROR;														// ERRO!			
			
			// Cria indice "."
			newRecords[0].TypeVal = 2;										
			strcpy(newRecords[0].name,".");
			newRecords[0].blocksFileSize = 1;
			newRecords[0].bytesFileSize = SECTORS_PER_BLOCK * SECTOR_SIZE;
			newRecords[0].dataPtr[0] = freeBlock;
			newRecords[0].dataPtr[1] = 0;
			newRecords[0].singleIndPtr = 0;
			newRecords[0].doubleIndPtr = 0; 

			// Cria indice ".."
			newRecords[1].TypeVal = 2;
			strcpy(newRecords[1].name,"..");
			newRecords[1].blocksFileSize = 1;
			newRecords[1].bytesFileSize = SECTORS_PER_BLOCK * SECTOR_SIZE;
			newRecords[1].dataPtr[0] = path;
			newRecords[1].dataPtr[1] = 0;
			newRecords[1].singleIndPtr = 0;
			newRecords[1].doubleIndPtr = 0;

			// Cria demais indices invalidados
            for(j=2; j < superblock.nOfDirEntries; j++){
			    newRecords[j].TypeVal = 0;
			    strcpy(newRecords[j].name,"");
			    newRecords[j].blocksFileSize = 0;
			    newRecords[j].bytesFileSize = 0;
			    newRecords[j].dataPtr[0] = 0;
			    newRecords[j].dataPtr[1] = 0;
			    newRecords[j].singleIndPtr = 0;
			    newRecords[j].doubleIndPtr = 0;
            }

			write_records_per_block(freeBlock,newRecords);					// salva novo records no disco		

			return 0;
		}
	}

	return FULL_DIRECTORY_ERROR;							// ERRO!	
}

int rmdir2 (char *pathname){
	int path;
	int i,j;	
	t2fs_record_t records[RECORDS_PER_BLOCK];
	int isDeleted, isEmpty, isInitial = 0;
	int depth = 0;

	// TODO: separar o filename por /
	// TODO: tratar caso de . 
	// TODO: tratar caso de ..
	// NOTE: isso seria implementado pela pathnameToPath()

	path = currentPath;
	read_records_per_block(path,records); 	

	// anda ateh o diretorio mais interno
	for(j=2; j < superblock.nOfDirEntries;j++){
		isEmpty = 1;

		// Abaixo sera simulado o comportamento de recursao
		// na qual, os pontos de parada sao quando:
		// -> O diretorio nao eh o que esta sendo buscado
		// -> A profundidade eh 0 (ou seja, voltou para a origem) 

		if((records[j].TypeVal == 2 && strcmp(records[j].name, pathname)==0) || depth > 0){	
			isInitial = 1;												            // comecou a processar a remocao
            if(records[j].TypeVal == 2 && strcmp(records[j].name, pathname)==0)     // se entrou no diretorio porque reconheceu o nome
    			depth++;													        // aumenta o contador de profundidade

			read_records_per_block(records[j].dataPtr[0],records); 			        // le records (entra no diretorio)
			
			for(i=2; i < superblock.nOfDirEntries; i++){	    // anda pelos records apos o "." e o ".."
				if(records[i].TypeVal != 0){					// se existe uma entrada valida
					if(records[i].TypeVal == 1){				// e eh um arquivo
						isDeleted = delete2(records[i].name);	// apaga o arquivo
						isEmpty = (isDeleted == 0 ? 1 : 0);		// se nao apagar, flag de vazio DESLIGA
					}
					else{										// se eh um diretorio
						isEmpty = 0;							// DESLIGA flag de vazio
						break;									// quebra o laco de varificacao
					}
				}
			}

			if(isEmpty == 1){															// se esta vazio (soh com "." e "..")	    
				read_records_per_block(records[1].dataPtr[0],records);					// volta para o diretorio anterior pelo ".."								
				depth--;																// diminui o contador de profundidade
				isDeleted = deallocBlocksForRecord(records[0].dataPtr[0],records[j]);	// apagar records recem esvaziado								
            	if(isDeleted != 0)														// se nao apagou
            		return FREE_RECORD_ERROR;											// ERRO!
            	
            	read_records_per_block(records[0].dataPtr[0],records); 	            	// pega records atualizados do disco            	

	            // XXX: deveria mandar a posição dentro do bloco para atualizar o record
	            //      mas como não tenho muito tempo e já estou tonto após programar 20h nas últimas 48h
	            //      vou simplesmente apagar o nome do record aqui e gravar o bloco todo novamente
	            strcpy(records[j].name,"");             							// apaga nome do record
	            isDeleted = write_records_per_block(records[0].dataPtr[0],records); // escreve record no disco	            
	            if(isDeleted != 0)													// se nao escreveu no disco
            		return WRITE_RECORD_BLOCK_ERROR;								// ERRO!            	
			}
			else {		              
				read_records_per_block(records[i].dataPtr[0],records);		// entra em records[j]
				depth++;													// aumenta o contador de profundidade
				j = 2;														// vai andar mais um bloco de records
			}
		}	
	}

	if(depth == 0 && isInitial == 1)			// se saiu do laco porque voltou para a origem e ja processou tudo
		return 0;								// SUCESSO!
												
	return DIRECTORY_NOT_FOUND_ERROR;			// ERRO!
}

DIR2 opendir2 (char *pathname){
	return 0;
}

int readdir2 (DIR2 handle, DIRENT2 *dentry){
	return 0;
}

int closedir2 (DIR2 handle){
	return 0;
}

int chdir2 (char *pathname){
    char* paths;
    char* pathnameCopy;
    t2fs_record_t records[RECORDS_PER_BLOCK];
    int isInvalidPath = 0;
    int partialPath;
    int i;
    
    pathnameCopy = malloc(sizeof(pathname));
    paths = malloc(sizeof(pathname));
    partialPath = currentPath;
    
    if(pathname[0] == '/')
        partialPath = superblock.superBlockSize+superblock.bitmapSize;              // partialPath vai para o "/"

    if(strlen(pathname) == 1){
        currentPath = partialPath;
        return 0;
    }

    strcpy(pathnameCopy,pathname);
    paths = strtok(pathnameCopy,"/");  
    while (paths != NULL && isInvalidPath == 0)
    {   
        read_records_per_block(partialPath,records);                                // entra no diretorio
        for(i=0; i < superblock.nOfDirEntries; i++){    
            if(records[i].TypeVal == 2 && strcmp(records[i].name,paths) == 0){      // verificar existencia de dir     
                printf("!! %s\n",records[i].name);   
                partialPath = records[i].dataPtr[0];                                // altera diretorio parcial
                isInvalidPath = 0;                                                  // path eh valido
                paths = strtok (NULL, "/");                                         // muda paths para o proximo nome de diretorio
                break;
            }
            else                                                                    // se path ainda nao foi encontrado
                isInvalidPath = 1;                                                    // path eh invalido
        }            
    }

    if(isInvalidPath == 0)                                      // se o diretorio nao eh invalido
        currentPath = partialPath;                              // currentPath muda 

    return isInvalidPath;
}

int getcwd2 (char *pathname, int size){
	// TODO: verificar size <= tamanho de working_directory, senao retorna erro
	char rootPathname[] = "/";
    char pathnameCopy[] = "";
    int antPath = 0,nextPath; 
    int i;
    t2fs_record_t records[RECORDS_PER_BLOCK];

    if(strlen(pathname) > size)
        return -1;
    

    if(currentPath == superblock.superBlockSize + superblock.bitmapSize){
		memcpy(pathname,rootPathname,size);
        return 0;
    }

    antPath = currentPath;
    read_records_per_block(antPath,records);                   // le diretorio atual    
    antPath = records[1].dataPtr[0];        
    nextPath = records[0].dataPtr[0];   
    read_records_per_block(antPath,records);      // volta para o pai  

    while(nextPath != antPath){        
        for(i = 2; i < superblock.nOfDirEntries; i++){              // para cada diretorio apos "." e ".."
            if(records[i].dataPtr[0] == nextPath){                     // se aponta para o filho
                strcpy(pathnameCopy+1,records[i].name);
                pathnameCopy[0] = '/';
                strcat(pathnameCopy,pathname);
                strcpy(pathname,pathnameCopy);
                strcpy(pathnameCopy,"");
                break;
            }
        }  
        antPath = records[1].dataPtr[0];        
        nextPath = records[0].dataPtr[0];   
        read_records_per_block(antPath,records);      // volta para o pai  
     
    }

    printf("dsadsa\n");

	return 0;
}


// ------------------------------------------
//			Funcoes Auxiliares
// ------------------------------------------

// ------------------------------------------------------
// Calcula o bloco referente ao offset do current pointer
// Entrada: offset
//			record
// Saida:	sucesso -> bloco correspondente ao offset
//			falha	-> -1
// -------------------------------------------------------
int getBlockFromOffset(const int offset,const t2fs_record_t record){
	char* ptrContent;
	int block = -1;
	int singleIndIndex,doubleIndIndex;
    int isRight;

	printf("offset %d \n",offset);
	if(offset < superblock.blockSize*SECTOR_SIZE)
        if(record.dataPtr[0] == 0)
            printf("Alocar primeiro bloco direto");
        else
    		block = record.dataPtr[0];
	else if(offset < 2*superblock.blockSize*SECTOR_SIZE){
        if(record.dataPtr[1] == 0)
               printf("Alocar segundo bloco direto \n");
        else
    		block = record.dataPtr[1];
	}
	else if(offset < pow((superblock.blockSize*SECTOR_SIZE),2)){
		// TODO: REFATORAR ISSO EM OUTRA FUNCAO --> int getBlockFromIndPtr(offset,indPtr)
		//		 a funcao acima podera ser reaproveitada nos apontamentos duplos
		printf("Blocos indirecao simples #%d\n",record.singleIndPtr);
	    if(record.singleIndPtr == 0)
	    	printf("Bloco indireto simples nao alocado\n");
        else {
		    singleIndIndex = round(offset / (superblock.blockSize*SECTOR_SIZE))-2;	
            printf("index %d\n",singleIndIndex);
            ptrContent = malloc(superblock.blockSize*SECTOR_SIZE);	
            if(ptrContent == NULL){
                printf("Erro de alocação dinâmica\n");										// DEBUG
                return MALLOC_ERROR;
            }
		    isRight = read_block(record.singleIndPtr,ptrContent);
            if(isRight != 0){
                printf("Erro de leitura\n");												// DEBUG
                return READ_BLOCK_ERROR;
            }
	        if(ptrContent[singleIndIndex] > 0)
	        	block = ptrContent[singleIndIndex];
	        else																			// DEBUG
	            printf("Bloco #%d do bloco indireto simples nao alocado\n",singleIndIndex); // DEBUG           
        }
	}
	else if(offset < pow((superblock.blockSize*SECTOR_SIZE),3)){
		// TODO: procurar block em fileHandlers[handle]->record.doubleIndPtr
		printf("Blocos de indirecao dupla #%d\n",record.doubleIndPtr);
		if(record.doubleIndPtr == 0)
	    	printf("Bloco de indirecao dupla nao alocado\n");
        else {        	
        	// TODO: encontra o bloco indireto que possui o bloco direto buscado
        	
//-------------------------------- verificar o calculo abaixo
        	doubleIndIndex = round(offset / (superblock.blockSize*SECTOR_SIZE))-2;	//
// --------------------------------


            printf("index %d\n",doubleIndIndex);
            ptrContent = malloc(superblock.blockSize*SECTOR_SIZE);	
            if(ptrContent == NULL){
                printf("Erro de alocação dinâmica\n");
                return MALLOC_ERROR;
            }
		    isRight = read_block(record.doubleIndPtr,ptrContent);
            if(isRight != 0){
                printf("Erro de leitura\n");
                return -1;
            }
	        if(ptrContent[doubleIndIndex] == 0)
	            printf("Bloco #%d do bloco indireto simples nao alocado\n",doubleIndIndex);
            else{

// 00000000000000000000-------------       	procurar em blocos indiretos internos
				printf("Blocos indirecao simples #%d\n",ptrContent[doubleIndIndex]);
			    if(ptrContent[doubleIndIndex] == 0)
			    	printf("Bloco indireto simples nao alocado\n");
		        else {
//-------------------------------- verificar o calculo abaixo
				    singleIndIndex = round(offset / (superblock.blockSize*SECTOR_SIZE))-2;	
//-------------------------------- 
		            printf("index %d\n",singleIndIndex);
		            ptrContent = malloc(superblock.blockSize*SECTOR_SIZE);	
		            if(ptrContent == NULL){
		                printf("Erro de alocação dinâmica\n");
		                return MALLOC_ERROR;
		            }
				    isRight = read_block(record.singleIndPtr,ptrContent);
		            if(isRight != 0){
		                printf("Erro de leitura\n");
		                return -1;
		            }
			        if(ptrContent[singleIndIndex] == 0)
			            printf("Bloco #%d do bloco indireto simples nao alocado\n",singleIndIndex);
		            else
		    		    block = ptrContent[singleIndIndex];
		        }
// 000000000000000000000------------
            }
        }
	}
	// TODO: else -> colocar no final do arquivo

	return block;
}

// Esta função seria baseada na chdir2 para retornar o numero do bloco referente ao pathname
// ela iria auxiliar as demais funções para decomporem os parametros 
// e permitir criar arquivos ou diretorios em outros diretorios alem do raiz (/)
int pathnameToPath(char* pathname){   
    return 0;
}
