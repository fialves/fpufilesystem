#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/apidisk.h"
#include "../include/t2fs.h"
#include "../include/t2fs_driver.h"
#include "../include/t2fs_test.h"

void test_identify2(){
    char name[40];
    int isIdentified = identify2(name,40);
    if(isIdentified != 0) {
        printf("> Error on identify2()\n");
        exit(-1);
    }
    puts(name);
}

void test_read2(){
	int currentByte = 3;
	char* buffer;
	printf("> Abrindo arquivo 'teste.txt'\n");
	FILE2 handler =	open2("teste.txt");
	if(handler < 0) {
        printf("> Error on open2()\n");
        exit(-1);
    }	

    buffer = malloc(2*SECTORS_PER_BLOCK*SECTOR_SIZE);
    strcpy(buffer,"");
    printf("------------------------\n");
	printf("> Lendo arquivo 'teste.txt' ate byte %d\n",currentByte);
	int isRead = read2(handler,buffer,currentByte);
    if(isRead < 0) {
        printf("> Error on read2()\n");
        exit(-1);
    }
    puts(buffer);

    currentByte += 20;
    strcpy(buffer,"");
    printf("------------------------\n");
    printf("> Lendo arquivo 'teste.txt' ate byte %d(teste de offset)\n",currentByte);
    isRead = read2(handler,buffer,currentByte);
    if(isRead < 0) {
        printf("> Error on read2()\n");
        exit(-1);
    }
	puts(buffer);

    currentByte = SECTORS_PER_BLOCK*SECTOR_SIZE - currentByte;
    strcpy(buffer,"");
    printf("------------------------\n");
    printf("> Lendo arquivo 'teste.txt' ate byte %d(teste de offset no final do arquivo)\n",currentByte);
    isRead = read2(handler,buffer,currentByte);
    if(isRead < 0) {
        printf("> Error on read2()\n");
        exit(-1);
    }
    puts(buffer);

    currentByte = 1;
    seek2(handler,SECTORS_PER_BLOCK*SECTOR_SIZE-1); // POSICIONA cursor no final do primeiro bloco
    strcpy(buffer,"");
    printf("------------------------\n");
    printf("> Lendo arquivo 'teste.txt' ate byte %d(teste de offset apos final do arquivo)\n",currentByte);
    isRead = read2(handler,buffer,currentByte);
    if(isRead < 0) {
        printf("> Error on read2()\n");
        exit(-1);
    }
    puts(buffer);

    seek2(handler,0); // POSICIONA cursor no inicio
    strcpy(buffer,"");
    currentByte = 2*SECTORS_PER_BLOCK*SECTOR_SIZE; // tamanho de 1 bloco
    printf("------------------------\n");
    printf("> Lendo arquivo 'teste.txt' ate byte %d(desde o inicio)\n",currentByte);
    isRead = read2(handler,buffer,currentByte);
    if(isRead < 0) {
        printf("> Error on read2()\n");
        exit(-1);
    }
    puts(buffer);
}

void test_create2(){
	int isCreated = create2("teste2.txt");
	if(isCreated < -1){
        printf("> [%d]Error on create2()\n",isCreated);
        exit(isCreated);
	}	
	FILE2 handler;
	if(isCreated == -1){
		printf("> Arquivo já existe!\n");
		printf("> Abrindo arquivo 'teste2.txt'\n");
		handler = open2("teste2.txt");
		if(handler < 0) {
		    printf("> Error on open2()\n");
		    exit(handler);
		}	
	}
	else 
		handler = isCreated;
	
	// TODO: testar a escrita no arquivo já criado

	char* buffer;
    buffer = malloc(SECTORS_PER_BLOCK*SECTOR_SIZE);
	int currentByte = SECTORS_PER_BLOCK*SECTOR_SIZE;
	printf("> Lendo arquivo 'teste2.txt' ate byte %d\n",currentByte);
	int isRead = read2(handler,buffer,currentByte);
	if(isRead < 0) {
	    printf("> [%d]Error on read2()\n",isRead);
	    exit(isRead);
	}
	puts(buffer);
}

void test_write2(){
    int currentByte = 3072;
    int i;
    char* buffer;

    printf("> Abrindo arquivo 'teste.txt'\n");
    FILE2 handler = open2("teste.txt");
    if(handler < 0) {
        printf("> Error on open2()\n");
        exit(-1);
    }   
    buffer = malloc(2048);
    printf("> Lendo arquivo 'teste.txt' ate byte %d\n",currentByte);
    int isRead = read2(handler,buffer,currentByte);
    if(isRead < 0) {
        printf("> Error on read2()\n");
        exit(-1);
    }
    puts(buffer);
    
    printf("> Escrevendo em arquivo 'teste.txt'(teste de offset)\n");    
    // seek2(handler,1024); 
    for(i=0;i < 5;i++){
        strcpy(buffer,"AAA!");        
        currentByte = strlen(buffer);
        seek2(handler,5);        
        int isWrote = write2(handler,buffer,currentByte);
        if(isWrote < 0) {
            printf("> Error on wrote2()\n");
            exit(isWrote);
        }        
        printf("-------- Next ---------\n");
    }
    seek2(handler,0);
    strcpy(buffer,"\n");
    printf("> Lendo arquivo 'teste.txt' ate byte %d(teste de offset no final do arquivo)\n",currentByte);
    isRead = read2(handler,buffer,3*SECTORS_PER_BLOCK*SECTOR_SIZE); // le 3 blocos
    if(isRead < 0) {
        printf("> Error on read2()\n");
        exit(-1);
    }

    puts(buffer);
}

void test_mkdir2(){
    int isCreated,isDeleted;

    printf("----- Teste do mkdir2 -----\n");
    printf("> Criando diretorio \"/NovoDir\"\n");
    isCreated = mkdir2("NovoDir");
    if(isCreated == 0){
        printf("> Diretorio \"/NovoDir\" criado com sucesso!\n");
        /*chdir2("NovoDir");    
        isCreated = mkdir2("NovoDir2");
        if(isCreated == 0)
            printf("> Diretorio \"/NovoDir/NovoDir2\" criado com sucesso!\n");
        else
            printf("> Falha ao criar o diretorio \"/NovoDir/NovoDir2\"!\n");*/
    }
    else
        printf("> Falha ao criar o diretorio \"/NovoDir\"!\n");

    // Limpando diretorios criados no teste anterior
    chdir2("/");
    printf("> Removendo \"NovoDir\"\n");
    isDeleted = rmdir2("NovoDir");
    if(isDeleted == 0)
        printf("> Diretorio removido com sucesso!\n");
    else
        printf("> Falha ao remover o diretorio!\n");
    
}

void test_rmdir2(){
    int isDeleted;

    printf("----- Teste do rmdir2 -----\n");
    chdir2("/");
    printf("> Removendo \"NovoDir\"\n");
    isDeleted = rmdir2("NovoDir");
    if(isDeleted == 0)
        printf("> Diretorio removido com sucesso!\n");
    else
        printf("> Falha ao remover o diretorio!\n");
}

void test_chdir2(){
    int isChanged, isDeleted;

    printf("----- Teste do chdir2 -----\n");

    // TESTE DE ACESSO DE DOIS NIVEIS DE PROFUNDIDADE -> /NovoDir/NovoDir2 
    printf("> Mudando para \"/\"...\n");
    isChanged = chdir2("/");
    if(isChanged == 0){
    	//test_getcwd2();
        printf("> Criando diretorio \"/NovoDir\"...\n");
        mkdir2("NovoDir");
        printf("> Mudando para \"/NovoDir\"...\n");
        isChanged = chdir2("NovoDir");    
        if(isChanged==0){
        	//test_getcwd2();
            printf("> Criando diretorio \"/NovoDir/NovoDir2\"...\n");
            mkdir2("NovoDir2");                
            printf("> Mudando para \"/NovoDir/NovoDir2\"\n");
            isChanged = chdir2("NovoDir2");    
            if(isChanged==0){
            	//test_getcwd2();
                printf("> Mudou para diretorio \"/NovoDir/NovoDir2\"\n");
            }
            else {
                printf("> Falha ao mudar para diretorio \"/NovoDir/NovoDir2\"\n");
                return;
            }        
        }
        else {
            printf("> Falha ao mudar para diretorio \"/NovoDir\"\n");
            return;
        }        
    }
    else {
        printf("> Falha ao mudar para diretorio \"/\"\n");
        return;
    }
    
    // Limpando diretorios criados no teste anterior
    chdir2("/");
    printf("> Removendo \"NovoDir\"\n");
    isDeleted = rmdir2("NovoDir");
    if(isDeleted == 0)
        printf("> Diretorio removido com sucesso!\n");
    else
        printf("> Falha ao remover o diretorio!\n");

    // TESTANDO DIRETORIO INEXISTENTE
    printf("> Mudando para \"DirInexistente\"\n");
    isChanged = chdir2("DirInexistente");
    if(isChanged == 0)
        printf("> Mudou para diretorio \"DirInexistente\"\n");
    else
        printf("> Falha ao mudar de diretorio(comportamento esperado)\n");        

}

void test_getcwd2(){
	char pathname[20];
	int currentByte = 20;
//    chdir2("/NovoDir");
	int isGotCWD = getcwd2(pathname,currentByte);
	if(isGotCWD < 0) {
	    printf("> [%d]Error on getcwd2()\n",isGotCWD);
	    exit(isGotCWD);
	}
    printf("> Diretorio atual: ");
	puts(pathname);
}

void test_delete2(){
    int isDeleted;

    isDeleted = delete2("teste2.txt");
    if(isDeleted == 0)
        printf("> Arquivo \"teste2.txt\" removido com sucesso.\n");
    else
        printf("> Falha ao remover arquivo \"teste2.txt\".\n");
}

void test_superblock(){
	read_superblock();  // le superbloco

    printf("\n---------------------\nDEBUG: Superblock Information\n---------------------\n");
    /// FIXME: somehow there are some extra chars in superblock.Id
    printf("ID:%s\n",superblock.id);
    printf("Version:%x\n",superblock.version);
    printf("SuperblockSize:%x\n",superblock.superBlockSize);
    printf("BlockSize:%d\n",superblock.blockSize);
    printf("DiskSize:%d\n",superblock.diskSize);
    printf("NofSectors:%d\n",superblock.nOfSectors);
    printf("BitmapSize:%d\n",superblock.bitmapSize);
    printf("RootSize:%d\n\n",superblock.rootSize);    
    printf("NOfDirEntries:%d\n",superblock.nOfDirEntries);    
    printf("FileEntrySize:%d\n",superblock.fileEntrySize);    
}

void test_rootDirectory(){
	int i,j,recordIndex;
    int rootBaseSector = superblock.superBlockSize + superblock.bitmapSize; 
	t2fs_record_t records[RECORDS_PER_BLOCK];

	for(i=0;i < superblock.rootSize; i++){
		read_records_per_block(rootBaseSector+i,records); 	
		for(j=0; j< superblock.blockSize;j++){
			recordIndex = (i*superblock.blockSize)+j;
			printf("---- Record %d ----\n", recordIndex);
			printf("TypeVal: %d\n",records[j].TypeVal);
			printf("name: %s\n",records[j].name);
			printf("Reserved: %s\n",records[j].Reserved);
			printf("blockFileSize: %d\n",records[j].blocksFileSize);
			printf("bytesFileSize: %d\n",records[j].bytesFileSize);
			printf("dataPtr[0]: %d\n",records[j].dataPtr[0]);
			printf("dataPtr[1]: %d\n",records[j].dataPtr[1]);
			printf("singleIndPtr: %d\n",records[j].singleIndPtr);
			printf("doubleIndPtr: %d\n",records[j].doubleIndPtr);			
		}
	}
}
