#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../include/t2fs_driver.h"
#include "../include/bitmap2.h"
#include "../include/apidisk.h"

// Errors
#define WRITE_SECTOR_ERROR -1
#define READ_SECTOR_ERROR -1
#define WRITE_BLOCK_ERROR -2
#define READ_BLOCK_ERROR -2
#define ALLOC_ERROR -3
#define DEALLOC_ERROR -3
#define RECORD_NOT_FOUND -4
#define NO_FREE_BLOCK_ERROR -5
#define MALLOC_ERROR -6
#define MAX_BLOCKS_ERROR -7

int read_superblock(){
    char buffer[SECTOR_SIZE];
    int isRead = read_sector(0,&buffer[0]);

    if(isRead == 0){
        memcpy(&(superblock.id), &buffer[0], 4*sizeof(BYTE));
        if(superblock.id[0]=='T' && superblock.id[1]=='2' && superblock.id[2]=='F' && superblock.id[3]=='S'){
            memcpy(&(superblock.version), &buffer[4], 2*sizeof(BYTE));
            memcpy(&(superblock.superBlockSize), &buffer[6], 2*sizeof(BYTE));
            memcpy(&(superblock.blockSize), &buffer[8], 2*sizeof(BYTE));
            memcpy(&(superblock.diskSize), &buffer[10], 4*sizeof(BYTE));
            memcpy(&(superblock.nOfSectors), &buffer[14], 4*sizeof(BYTE));
            memcpy(&(superblock.bitmapSize), &buffer[18], 4*sizeof(BYTE));
            memcpy(&(superblock.rootSize), &buffer[22], 4*sizeof(BYTE));
			memcpy(&(superblock.nOfDirEntries), &buffer[26], 4*sizeof(BYTE));
            memcpy(&(superblock.fileEntrySize), &buffer[30], 4*sizeof(BYTE));
            currentPath = superblock.superBlockSize + superblock.bitmapSize; // currentPath = "/"
            // NOTE: there are more Bytes from &buffer[34] until the end of the superblock
        }
        return 0;
    }
    return -1;
}

// write paramBuffer in block
int write_block (unsigned int block, char *paramBuffer){
    int i;
    for(i=0; i < SECTORS_PER_BLOCK ; i++){        
        if(write_sector(BLOCK_TO_SECTORS(block)+i, paramBuffer+(i*SECTOR_SIZE)) != 0) return WRITE_SECTOR_ERROR;
    }
    return 0;
}

// read block and store in paramBuffer
int read_block (unsigned int block, char *paramBuffer){
    int i;
    for(i=0; i < SECTORS_PER_BLOCK ; i++){        
        if(read_sector(BLOCK_TO_SECTORS(block)+i, paramBuffer+(i*SECTOR_SIZE)) != 0) return READ_SECTOR_ERROR;
    }

    return 0;
}

// read reacords in a block and store in records
int read_records_per_block(unsigned int position,struct t2fs_record *records){
    // in: absolute position; integer represents block number
    // out: records per block

    char blockBuffer[superblock.blockSize*SECTOR_SIZE];
    int i = 0;

    int isRead = read_block(position,blockBuffer);                                      // le bloco de records
    if(isRead == 0){                                                                    // se conseguiu ler
        for(i=0; i < RECORDS_PER_BLOCK; i++){                                           // para cada record no bloco
			memcpy(&records[i], &blockBuffer[i*RECORD_SIZE], sizeof(t2fs_record_t));    // copia records para o buffer 
        }
        return 0;                                                                       // SUCESSO
    }
    else
        return READ_BLOCK_ERROR;             // ERRO na leitura do bloco

    return RECORD_NOT_FOUND;                // ERRO por record nao encontrado
}

int write_records_per_block(unsigned int position,struct t2fs_record *records){
    // in: absolute position; integer represents block number
    // out: records per block

    char blockBuffer[superblock.blockSize*SECTOR_SIZE];
    int i = 0;

	for(i=0; i < RECORDS_PER_BLOCK; i++){
		memcpy(&blockBuffer[i*RECORD_SIZE], &records[i], sizeof(t2fs_record_t));
    }

    int isWrote = write_block(position,blockBuffer);
    return isWrote;
}

int update_record(unsigned int position, struct t2fs_record record){
    int i;
    int isWrote;
    t2fs_record_t records[RECORDS_PER_BLOCK];
    
    read_records_per_block(position,records);                           // Le records em currentPath
    
    for(i=0; i< superblock.blockSize;i++){                              // procura pelo record dentro do bloco de records
        if(strcmp(records[i].name, record.name)==0) {                   // se records tem mesmo nome
            records[i] = record;                                        // altera o record            
            isWrote = write_records_per_block(position,records);        // salvar todos os records no disco
            return isWrote;                                             // retorna SUCESSO ou ERRO de escrita
        }
    }
    return RECORD_NOT_FOUND;            // RECORD NOT FOUND
}

int allocBlockForRecord(unsigned int position,struct t2fs_record record){
    char* ptrContent;
	int block = -1, freeBlock;
	int i;
    int isRight = 0;

    int totalBlocks = record.blocksFileSize;
	if(totalBlocks == 0){
        freeBlock = searchFreeBlock2(); // procura bloco livre
        allocBlock2(freeBlock);         // aloca bloco antes livre
        record.dataPtr[0] = freeBlock;  // insere bloco no record
        record.blocksFileSize++;        // atualiza contador de blocos
        update_record(position,record); // atualiza record no disco
		block = record.dataPtr[0];      // retorna novo bloco
    }
	else if(totalBlocks == 1){
        freeBlock = searchFreeBlock2();         // procura bloco livre
        if(freeBlock == 0){                     // se não encontra
            printf("Sem blocos livres");        // DEBUG
            return NO_FREE_BLOCK_ERROR;         // ERRO!
        }
        isRight = allocBlock2(freeBlock);       // aloca bloco antes livre
        if(isRight != 0){                       // se não alocar
            printf("Falha ao alocar bloco\n");  // DEBUG
            return ALLOC_ERROR;                 // ERRO!
        }
        record.dataPtr[1] = freeBlock;          // insere bloco no record
        record.blocksFileSize++;                // atualiza contador de blocos
		block = record.dataPtr[1];              // retorna bloco novo
        printf("Segundo bloco direto alocado com sucesso\n");   // DEBUG
	}
	else if(totalBlocks < pow((superblock.blockSize*SECTOR_SIZE)/sizeof(DWORD),2)){
		printf("Verificando blocos indiretos SIMPLES\n");
        ptrContent = malloc(superblock.blockSize*SECTOR_SIZE);          // Inicializa ptrContent
        if(ptrContent == NULL){                                         // senão
            printf("Falha de alocação dinamica\n");                     // DEBUG
            return MALLOC_ERROR;                                        // ERRO!
        }
	    if(record.singleIndPtr == 0){                                   // se bloco de indireção simples ainda não foi alocado
	    	printf("Alocando bloco indireto SIMPLES\n");
            freeBlock = searchFreeBlock2();                             // procura bloco livre
            if(freeBlock == 0){                                         // se não encontrar
                printf("Sem blocos livres\n");                          // DEBUG
                return NO_FREE_BLOCK_ERROR;                             // ERRO!
            }
            isRight = allocBlock2(freeBlock);                           // aloca bloco antes livre
            if(isRight != 0){                                           // se não alocar
                printf("Falha ao alocar bloco\n");                      // DEBUG
                return ALLOC_ERROR;                                     // ERRO!
            }
            record.singleIndPtr = freeBlock;                            // insere referencia de blocos de indireção simples
            printf("Bloco livre #%d\n",freeBlock);                      // DEBUG
            //record.blocksFileSize++;                                  // atualiza contador de blocos 
                                                                        // NOTE: acho que so deve contabilizar quando for byte de conteudo
            printf("Bloco escrito com sucesso!\n");                             
            // aloca primeiro bloco indireto simples
		    read_block(record.singleIndPtr,ptrContent);                 // ler bloco de indireção simples
            printf("conteudo\n %s\n",ptrContent);                       // DEBUG
            freeBlock = searchFreeBlock2();                             // procura bloco livre
            if(freeBlock == 0){                                         // se não encontrar
                printf("Sem blocos livres\n");                          // DEBUG
                return NO_FREE_BLOCK_ERROR;                             // ERRO!
            }
            printf("Bloco livre #%d\n",freeBlock);     
            isRight = allocBlock2(freeBlock);                           // aloca bloco antes livre
            if(isRight != 0){                                           // se não alocar
                printf("Falha ao alocar bloco\n");                      // DEBUG
                return ALLOC_ERROR;                                     // ERRO!
            }
            ptrContent[0] = freeBlock;                                  // insere primeiro bloco direto
            write_block(record.singleIndPtr,ptrContent);                // atualiza bloco de indireção simples no disco
            printf("Bloco escrito com sucesso!\n");     
            record.blocksFileSize++;                                    // atualiza contador de blocos
            printf("Bloco alocado com sucesso!\n");     
            block = ptrContent[0];                                      // retorna bloco novo

        } else {                                                                    // Se bloco de indireção simples já está alocado        
		    read_block(record.singleIndPtr,ptrContent);                             // ler bloco de indireção simples
            for(i=0; i < (superblock.blockSize*SECTOR_SIZE)/sizeof(DWORD); i++){    // para cada bloco direto
                if(ptrContent[i] == 0){                                             // se algum não estiver alocado
	                printf("Alocando bloco #%d do bloco indireto simples\n",i);     
                    freeBlock = searchFreeBlock2();                                 // procura bloco livre
                    allocBlock2(freeBlock);                                         // aloca bloco antes livre
                    ptrContent[i] = freeBlock;                                      // insere bloco indiretos simples
                    record.blocksFileSize++;                                        // atualiza contador de blocos                
                    write_block(record.singleIndPtr,ptrContent);                    // atualiza lista de referencias no disco

		            block = ptrContent[i];                                          // retorna bloco novo
                    break;                                                          // termina a busca
                }                
            }            
        }
	}
	else if(totalBlocks < pow((superblock.blockSize*SECTOR_SIZE)/sizeof(DWORD),3)){
		// TODO: alocar bloco em record.doubleIndPtr
		printf("Blocos duplamente indiretos\n");                                    // DEBUG
	}
    else{                                                                           // Se ultrapassar o limite de espaço
        printf("O arquivo atingiu o tamanho maximo\n");                             // DEBUG
        return MAX_BLOCKS_ERROR;                                                    // ERRO!
    }

    update_record(position,record);                                                 // atualiza record no disco
	return block;
}

// TODO: verificar funções usando isRight
int deallocBlocksForRecord(unsigned int position,struct t2fs_record record){
    char* ptrContent;
    char* ptrContent2;
    int isRight = 0;
    int i,j;

    if(record.dataPtr[0] > 0){
        // libera bloco direto
        isRight = freeBlock2(record.dataPtr[0]);
        if(isRight != 0){
            printf("Falha ao liberar primeiro bloco direto\n");         // DEBUG
            return DEALLOC_ERROR;                                       // ERRO!
        }
        record.dataPtr[0] = 0;
        record.blocksFileSize--;
    }
    if(record.dataPtr[1] > 0){        
        isRight = freeBlock2(record.dataPtr[1]);                        // libera bloco direto
        if(isRight != 0){                                               // senao
            printf("Falha ao liberar segundo bloco direto\n");          // DEBUG
            return DEALLOC_ERROR;                                       // ERRO!
        }
        record.dataPtr[1] = 0;
        record.blocksFileSize--;
    }
    if(record.singleIndPtr > 0){                                        // se o bloco de indireção simples está alocado       
        ptrContent = malloc(superblock.blockSize*SECTOR_SIZE);          // Inicializa ptrContent
        if(ptrContent == NULL){                                         // senão
            printf("Falha de alocação dinamica\n");                     // ERRO!
            return -2;
        }
        isRight = read_block(record.singleIndPtr,ptrContent);           // le bloco de indireção simples
        if(isRight != 0){                                               // senao
            printf("Falha ao ler bloco\n");                             // DEBUG
            return READ_BLOCK_ERROR;                                    // ERRO!
        }
        for(i=0; i < (superblock.blockSize*SECTOR_SIZE)/sizeof(DWORD); i++){            // para cada bloco direto
            if(ptrContent[i] > 0){                                                      // se está alocado
                freeBlock2(ptrContent[i]);                                              // libera o bloco
                ptrContent[i] = 0;                                                      // aponta para bloco invalido
                record.blocksFileSize--;                                                // decrementa total de blocos
            }
        }
        freeBlock2(record.singleIndPtr);                                                // libera o bloco de indireção simples
        record.singleIndPtr = 0;
    }
    if(record.doubleIndPtr > 0){                                                        // se o bloco de indireção dupla está alocado
        ptrContent = malloc(superblock.blockSize*SECTOR_SIZE);                          // Inicializa ptrContent
        ptrContent2 = malloc(superblock.blockSize*SECTOR_SIZE);                         // Inicializa ptrContent2
        if(ptrContent == NULL || ptrContent2 == NULL){                                  // senão
            printf("Falha de alocação dinamica\n");                                     // DEBUG
            return MALLOC_ERROR;                                                        // ERRO!
        }
        read_block(record.doubleIndPtr,ptrContent);                                     // le bloco de indireção dupla

        for(i=0; i < (superblock.blockSize*SECTOR_SIZE)/sizeof(DWORD); i++){            // para cada bloco de indireção simples
            if(ptrContent[i] > 0){                                                      // se está alocado
                read_block(ptrContent[i],ptrContent2);                                  // le bloco direto
                for(j=0; j < (superblock.blockSize*SECTOR_SIZE)/sizeof(DWORD); j++){    // para cada bloco direto
                    if(ptrContent2[j] > 0){                                             // se está alocado
                        freeBlock2(ptrContent2[j]);                                     // libera o bloco
                        ptrContent2[j] = 0;                                             // aponta para bloco invalido
                        record.blocksFileSize--;                                        // decrementa total de blocos
                    }
                }
                freeBlock2(ptrContent[i]);                                              // libera bloco de indireção simples
                ptrContent[i] = 0;                                                      // aponta para bloco invalido
            }
        }
        freeBlock2(record.doubleIndPtr);                                                // libera bloco de indireção dupla
        record.doubleIndPtr = 0;                                                        // aponta para bloco invalido
    }
    
    // XXX: para manter a coerencia, o contador de bytes deveria decrementar conforme o tamanho dos blocos livre
    record.bytesFileSize = 0;                                                           // zera o tamanho do arquivo
    record.TypeVal = 0;                                                                 // coloca tipo indefinido para o record
    //strcpy(record.name,"");                                                           // remove o nome do record (removido porque update_record precisa de name)
    update_record(position,record);                                                     // atualiza record no disco           
    
    return 0;                                                                           // SUCESSO
}
