#include <stdio.h>
#include "../include/t2fs.h"
#include "../include/t2fs_test.h"

int main(){	
	// Identify2 test
    test_identify2();

    // Superblock test	
	test_superblock();

    // Root directory test
    test_rootDirectory();

	// Open2 and read2 test
	test_read2();

	// Create2 test
	test_create2();
	
	// Write2 test
	test_write2();

    // Delete2 test
    test_delete2();
    test_rootDirectory(); // verificando se realmente apagou o record

    // Mkdir2 test
    test_mkdir2();
    test_rootDirectory();
    test_chdir2();

	// Getcwd2 test
	test_getcwd2();

    // Rmdir2 test
    test_rmdir2();
    test_rootDirectory();

	return 0;
}
