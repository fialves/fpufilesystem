#include "../include/t2fs.h"

#define RECORD_SIZE 64 // TODO: alterar conforme disco
#define ADDRESS_ABSOLUTE_RECORD(a) (superblock.superBlockSize+a)

#define RECORDS_PER_BLOCK superblock.blockSize*SECTOR_SIZE/RECORD_SIZE
#define SECTORS_PER_BLOCK (superblock.blockSize)
#define BLOCK_TO_SECTORS(block) ((block*SECTORS_PER_BLOCK))

typedef struct t2fs_record t2fs_record_t;

struct current_pointer2 {
	t2fs_record_t record;
	int offset;
} typedef current_pointer2_t;

struct t2fs_superbloco superblock;
current_pointer2_t* fileHandlers[20];
DIR2 currentPath;

// TODO: documentar
int read_superblock();
int write_block (unsigned int block, char *paramBuffer);
int read_block (unsigned int block, char *paramBuffer);
int read_records_per_block(unsigned int position,struct t2fs_record *records);
int write_records_per_block(unsigned int position,struct t2fs_record *records);
int update_record(unsigned int position, struct t2fs_record record);
int allocBlockForRecord(unsigned int position,struct t2fs_record record);
int deallocBlocksForRecord(unsigned int position,struct t2fs_record record);
